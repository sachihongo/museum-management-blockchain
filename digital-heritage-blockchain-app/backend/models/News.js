const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let newsSchema = new Schema({
  _id: mongoose.Schema.Types.ObjectId,
  judul: {
    type: String
  },
  gambar: {
    type: String
  },
  deskripsi: {
      type: String
  },
}, {
    collection: 'news',
    timestamps: true
  })

module.exports = mongoose.model('News', newsSchema)
