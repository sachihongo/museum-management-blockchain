let express = require('express'),
  multer = require('multer'),
  mongoose = require('mongoose'),
  router = express.Router();


// Multer File upload settings
const DIR = './public/berita/';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, fileName)
  }
});


// Multer Mime Type Validation
var upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
    }
  }
});


// News model
let News = require('../models/News');


// POST News
router.post('/create-news', upload.single('gambar'), (req, res, next) => {
  const url = req.protocol + '://' + req.get('host')
  const news = new News({
    _id: new mongoose.Types.ObjectId(),
    judul: req.body.judul,
    gambar: url + '/public/berita/' + req.file.filename,
    deskripsi: req.body.deskripsi,
  });
  news.save().then(result => {
    console.log(result);
    res.status(201).json({
      message: "News registered successfully!",
      newsCreated: {
        _id: result._id,
        judul: result.judul,
        gambar: result.gambar,
        deskripsi: result.deskripsi
      }
    })
  }).catch(err => {
    console.log(err),
      res.status(500).json({
        error: err
      });
  })
});

// Update News
router.put('/update-news/:id', (req, res, next) => {
  const url = req.protocol + '://' + req.get('host')

  News.findByIdAndUpdate(req.params.id, {
    judul: req.body.judul,
    deskripsi: req.body.deskripsi,
  })
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "News updated successfully!",
        // newsCreated: {
        //   _id: result._id,
        //   judul: result.judul,
        //   gambar: result.gambar,
        //   deskripsi: result.deskripsi
        // }
      })
    }).catch(err => {
      console.log(err),
        res.status(500).json({
          error: err
        });
    })
});


// GET All News
router.get("/", (req, res, next) => {
  News.find().then(data => {
    res.status(200).json({
      message: "News retrieved successfully!",
      news: data
    });
  });
});


// GET Single News
router.get("/:id", (req, res, next) => {
  News.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// DELETE News
router.delete("/delete-news/:id", (req, res, next) => {
  News.findByIdAndRemove(req.params.id).then(data => {
    if (data) {
      res.status(200).json(post);
    } else {
      res.status(404).json({
        message: "News not found!"
      });
    }
  });
});

module.exports = router;
