/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AngularTestPage } from './app.po';
import { ExpectedConditions, browser, element, by } from 'protractor';
import {} from 'jasmine';


describe('Starting tests for digital-heritage-blockchain-app', function() {
  let page: AngularTestPage;

  beforeEach(() => {
    page = new AngularTestPage();
  });

  it('website title should be digital-heritage-blockchain-app', () => {
    page.navigateTo('/');
    return browser.getTitle().then((result)=>{
      expect(result).toBe('digital-heritage-blockchain-app');
    })
  });

  it('network-name should be digital-heritage-blockchain@0.0.1',() => {
    element(by.css('.network-name')).getWebElement()
    .then((webElement) => {
      return webElement.getText();
    })
    .then((txt) => {
      expect(txt).toBe('digital-heritage-blockchain@0.0.1.bna');
    });
  });

  it('navbar-brand should be digital-heritage-blockchain-app',() => {
    element(by.css('.navbar-brand')).getWebElement()
    .then((webElement) => {
      return webElement.getText();
    })
    .then((txt) => {
      expect(txt).toBe('digital-heritage-blockchain-app');
    });
  });

  
    it('Koleksi component should be loadable',() => {
      page.navigateTo('/Koleksi');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Koleksi');
      });
    });

    it('Koleksi table should have 9 columns',() => {
      page.navigateTo('/Koleksi');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(9); // Addition of 1 for 'Action' column
      });
    });
  

  
    it('Kolektor component should be loadable',() => {
      page.navigateTo('/Kolektor');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Kolektor');
      });
    });

    it('Kolektor table should have 6 columns',() => {
      page.navigateTo('/Kolektor');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(6); // Addition of 1 for 'Action' column
      });
    });
  
    it('Pewaris component should be loadable',() => {
      page.navigateTo('/Pewaris');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Pewaris');
      });
    });

    it('Pewaris table should have 6 columns',() => {
      page.navigateTo('/Pewaris');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(6); // Addition of 1 for 'Action' column
      });
    });
  
    it('Organisasi component should be loadable',() => {
      page.navigateTo('/Organisasi');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Organisasi');
      });
    });

    it('Organisasi table should have 5 columns',() => {
      page.navigateTo('/Organisasi');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(5); // Addition of 1 for 'Action' column
      });
    });
  
    it('Pengguna component should be loadable',() => {
      page.navigateTo('/Pengguna');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Pengguna');
      });
    });

    it('Pengguna table should have 6 columns',() => {
      page.navigateTo('/Pengguna');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(6); // Addition of 1 for 'Action' column
      });
    });
  

  
    it('PindahKoleksi component should be loadable',() => {
      page.navigateTo('/PindahKoleksi');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('PindahKoleksi');
      });
    });
  

});