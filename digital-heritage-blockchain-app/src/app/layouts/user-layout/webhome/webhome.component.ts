import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-webhome',
  templateUrl: './webhome.component.html',
  styleUrls: ['./webhome.component.css']
})
export class WebhomeComponent implements OnInit {
  
  constructor(private router: Router) { }

  ngOnInit() {
 
   this.loadScript('../../../assets/js/user-layout/jquery-1.12.4.min.js');
    this.loadScript('../../../assets/js/user-layout/angular.min.js');
    this.loadScript('../../../assets/js/user-layout/modernizr.custom.js');
    this.loadScript('../../../assets/js/user-layout/cbpSplitLayout.js');
    this.loadScript('../../../assets/js/user-layout/app.js');
    this.loadScript('../../../assets/js/user-layout/controllers.js');
    this.loadScript('../../../assets/js/user-layout/notifyMe.js');
    this.loadScript('../../../assets/js/user-layout/jquery.countdown.js');
    this.loadScript('../../../assets/js/user-layout/jquery.placeholder.js');
    this.loadScript('../../../assets/js/user-layout/photoswipe.min.js');
    this.loadScript('../../../assets/js/user-layout/photoswipe-ui-default.min.js');
    this.loadScript('../../../assets/js/user-layout/jquery.lettering.js');
    // this.loadScript('../../../assets/js/user-layouth/jquery.textillate.js');
    // this.loadScript('../../../assets/js/user-layout/dotty.js');
    // this.loadScript('../../../assets/js/user-layout/jquery.nicescroll.js');
    // this.loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyBxXt2P7-U38bK0xEFIT-ebZJ1ngK8wjww');
    this.loadScript('../../../assets/js/user-layout/init.js');

     
  }

  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);

  }

  goToBerita(){
    this.router.navigate(['/webberita']);
  }

}
