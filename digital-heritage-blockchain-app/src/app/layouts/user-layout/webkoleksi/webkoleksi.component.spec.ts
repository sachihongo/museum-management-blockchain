import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebkoleksiComponent } from './webkoleksi.component';

describe('WebkoleksiComponent', () => {
  let component: WebkoleksiComponent;
  let fixture: ComponentFixture<WebkoleksiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebkoleksiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebkoleksiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
