import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KoleksiService } from './koleksi-user.service';

@Component({
  selector: 'app-webkoleksi',
  templateUrl: './webkoleksi.component.html',
  styleUrls: [
    './bootstrap.min.css',
    './webkoleksi.component.css',
  ],
  providers: [KoleksiService],
  encapsulation: ViewEncapsulation.None,
})
export class WebkoleksiComponent implements OnInit {

  id: any;
  private errorMessage;
  private allAssets;
  private asset;
  gambar: string;
  judul: string;
  deskripsi: string;

  constructor(public serviceKoleksi: KoleksiService, private route: ActivatedRoute) {
   
  }

  ngOnInit(){
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('Halo' + this.id);
    // console.log('halo' +  this.id );
    this.loadData(this.id);
  }

 
  loadData(id): Promise<any> {
    switch (id) {
      case 'historika':
        console.log('Load Data Historika');
        const tempList1 = [];
        this.gambar = '../../../../assets/img/koleksi/historika.png';
        this.judul = 'Koleksi Historika';
        this.deskripsi = 'Koleksi Historika adalah benda yang menjadi obyek penelitian sejarah. Koleksi yang digolongkan historika antara lain koleksi pedang dan meriam.';
        return this.serviceKoleksi.getAllHistorika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList1.push(asset);
            });
            this.allAssets = tempList1;
            console.log('keluarkan: ' + this.allAssets);
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });

      case 'etnografika':
        console.log('Load Data Etnografika');
        const tempList2 = [];
        this.gambar = '../../../../assets/img/koleksi/etnografika.png';
        this.judul = 'Koleksi Etnografika';
        this.deskripsi = 'Koleksi Etnografika adalah benda yang menjadi obyek ethnografika. koleksi etnografika terdiri dari berbagai hasil warisan budaya dari suatu etnis seperti wadah, wayang, dan keris.';
        return this.serviceKoleksi.getAllEtnografika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList2.push(asset);
            });
            this.allAssets = tempList2;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'grafika':
        console.log('Load Data Grafika ');
        const tempList3 = [];
        this.gambar = '../../../../assets/img/koleksi/grafika.png';
        this.judul = 'Koleksi Grafika';
        this.deskripsi = 'Koleksi Grafika adalah benda yang memberi representasi visual pada sebuah permukaan seperti dinding, kanvas, kertas, atau batu bertujuan untuk memberi tanda, informasi, ilustrasi, atau untuk hiburan.';
        return this.serviceKoleksi.getAllGrafika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList3.push(asset);
            });
            console.log('keluarkan: ' + tempList3);
            this.allAssets = tempList3;
           
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'senirupa':
        console.log('Load Data Seni Rupa');
        const tempList4 = [];
        this.gambar = '../../../../assets/img/koleksi/senirupa.png';
        this.judul = 'Koleksi Seni Rupa';
        this.deskripsi = 'Koleksi Seni Rupa adalah benda yang berupa lukisan ataupun yang lain. Koleksi seni rupa merupakan berbagai ekspresi artistik yang dituangkan dalam media dua maupun tiga dimensi.';
        return this.serviceKoleksi.getAllSeniRupa()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList4.push(asset);
            });
            this.allAssets = tempList4;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'numismatika':
        console.log('Load Data Numismatika');
        const tempList5 = [];
        this.gambar = '../../../../assets/img/koleksi/numismatika.png';
        this.judul = 'Koleksi Numismatika';
        this.deskripsi = 'Koleksi Numismatika adalah benda yang berupa alat tukar/uang. Jenis koleksi ini antara lain uang logam dan kertas, tanda jasa, lambang dan pangkat resmi, cap dan stempel.';
        return this.serviceKoleksi.getAllNumismatika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList5.push(asset);
            });
            this.allAssets = tempList5;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'biologika':
        console.log('Load Data Biologika');
        const tempList6 = [];
        this.gambar = '../../../../assets/img/koleksi/biologika.png';
        this.judul = 'Koleksi Biologika';
        this.deskripsi = 'Koleksi Biologika adalah benda yang menjadi obyek penelitian biologi. Koleksi biologika yang ada antara lain tengkorak dan kerangka manusia, tumbuh-tumbuhan, dan binatang.';
        return this.serviceKoleksi.getAllBiologika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList6.push(asset);
            });
            this.allAssets = tempList6;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'filologika':
        console.log('Load Data Filologika');
        const tempList7 = [];
        this.gambar = '../../../../assets/img/koleksi/filologika.png';
        this.judul = 'Koleksi Filologika';
        this.deskripsi = 'Koleksi Filologika adalah benda yang berupa naskah tulis. Jenis koleksi filologika meliputi berbagai naskah kuno';
        return this.serviceKoleksi.getAllFilologika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList7.push(asset);
            });
            this.allAssets = tempList7;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'arkeologika':
        console.log('Load Data Arkeologika');
        const tempList8 = [];
        this.gambar = '../../../../assets/img/koleksi/arkeologika.png';
        this.judul = 'Koleksi Arkeologika';
        this.deskripsi = 'Koleksi Arkeologika adalah benda yang menjadi obyek penelitian arkeologi. Benda-benda arkeologika yang ada terdiri dari kapak batu, gerabah, kalung manik, arca, dan artefak.';
        return this.serviceKoleksi.getAllArkeologika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList8.push(asset);
            });
            this.allAssets = tempList8;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'keramologika':
        console.log('Load Data Keramologika');
        const tempList9 = [];
        this.gambar = '../../../../assets/img/koleksi/keramologika.png';
        this.judul = 'Koleksi Keramologika';
        this.deskripsi = 'Koleksi Historika adalah benda yang berupa keramik atau gerabah. Jenis koleksi ini dibuat dari tanah dan dikeraskan dengan metode pembakaran tertentu.';
        return this.serviceKoleksi.getAllKeramologika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList9.push(asset);
            });
            this.allAssets = tempList9;
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'geografika':
        console.log('Load Data Geografika');
        const tempList10 = [];
        this.gambar = '../../../../assets/img/koleksi/geografika.png';
        this.judul = 'Koleksi Geografika';
        this.deskripsi = 'Koleksi Geografika adalah benda yang menjadi sarana referensi informasi mengenai letak suatu daerah, keadaan politik, perdagangan, hasil tambang, bahasa dan sebagainya.';
        return this.serviceKoleksi.getAllGeografika()
          .toPromise()
          .then((result) => {
            this.errorMessage = null;
            result.forEach(asset => {
              tempList10.push(asset);
            });
            this.allAssets = tempList10;
            console.log('keluarkan: ' + this.allAssets);
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      default:
        console.log('Salah Parameter');
    }
  } 


  Status: string;
  link = 'http://localhost:8000/public/koleksi/'


  getSingleClick(id: any): Promise<any> {
    return this.serviceKoleksi.getAsset(id)
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        this.Status = this.link + result.status;
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }


}
