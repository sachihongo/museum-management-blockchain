import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-webmenu',
  templateUrl: './webmenu.component.html',
  styleUrls: ['./webmenu.component.css']
})
export class WebmenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToBerita(){
    this.router.navigate(['/webberita']);
  }
}
