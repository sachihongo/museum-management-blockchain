import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebberitaComponent } from './webberita.component';

describe('WebberitaComponent', () => {
  let component: WebberitaComponent;
  let fixture: ComponentFixture<WebberitaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebberitaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebberitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
