import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-webberita',
  templateUrl: './webberita.component.html',
  styleUrls: ['./webberita.component.css']
})
export class WebberitaComponent implements OnInit {

  baseURL = "http://localhost:8000/api";

  News: any = [];

  constructor(private http: HttpClient) {
    this.getNews();
  }

  ngOnInit() {
  }

  getNews() {
    this.getAll().subscribe((res) => {
      this.News = res['news'];
    })
  }

  getAll() {
    return this.http.get(this.baseURL);
  }


}
