import { Routes } from '@angular/router';

import { HomeComponent } from '../../home/home.component';
import { KoleksiComponent } from '../../Koleksi/Koleksi.component';

import { KolektorComponent } from '../../Kolektor/Kolektor.component';
import { PewarisComponent } from '../../Pewaris/Pewaris.component';
import { OrganisasiComponent } from '../../Organisasi/Organisasi.component';
import { PenggunaComponent } from '../../Pengguna/Pengguna.component';

import { PindahKoleksiComponent } from '../../PindahKoleksi/PindahKoleksi.component';
import { BeritaComponent } from '../../berita/berita.component';
import { AddBeritaComponent } from '../../add-berita/add-berita.component';
import { HolderComponent } from '../../holder/holder.component';
import { AllTransaksiComponent } from '../../all-transaksi/all-transaksi.component';



export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: HomeComponent },
    { path: 'berita',         component: BeritaComponent },
    { path: 'addberita',      component: AddBeritaComponent },
    { path: 'holder',         component: HolderComponent },
    { path: 'kolektor',       component: KolektorComponent },
    { path: 'pewaris',        component: PewarisComponent },
    { path: 'organisasi',     component: OrganisasiComponent },
    { path: 'koleksi',        component: KoleksiComponent },
    { path: 'pindahkoleksi',  component: PindahKoleksiComponent },
    { path: 'alltransaksi',   component: AllTransaksiComponent }
];
