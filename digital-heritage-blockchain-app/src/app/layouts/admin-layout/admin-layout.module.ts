import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LbdModule } from '../../lbd/lbd.module';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { HomeComponent } from '../../home/home.component';
import { KoleksiComponent } from '../../Koleksi/Koleksi.component';

import { KolektorComponent } from '../../Kolektor/Kolektor.component';
import { PewarisComponent } from '../../Pewaris/Pewaris.component';
import { OrganisasiComponent } from '../../Organisasi/Organisasi.component';
import { PenggunaComponent } from '../../Pengguna/Pengguna.component';

import { DataService } from '../../data.service';
import { FileUploadService } from '../../berita/file-upload.service'

import { PindahKoleksiComponent } from '../../PindahKoleksi/PindahKoleksi.component';
import { BeritaComponent } from '../../berita/berita.component';
import { AddBeritaComponent } from '../../add-berita/add-berita.component';
import { HolderComponent } from '../../holder/holder.component';
import { AllTransaksiComponent } from '../../all-transaksi/all-transaksi.component';

import { FileSelectDirective } from 'ng2-file-upload';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    LbdModule,
    HttpClientModule
  ],
  declarations: [
    HomeComponent,
    KoleksiComponent,
    KolektorComponent,
    PewarisComponent,
    OrganisasiComponent,
    PenggunaComponent,
    PindahKoleksiComponent,
    BeritaComponent,
    AddBeritaComponent,
    HolderComponent,
    AllTransaksiComponent,
    FileSelectDirective
  ],
  providers: [
    DataService,
    FileUploadService
  ],
})

export class AdminLayoutModule {}
