import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { FileUploadService } from "../berita/file-upload.service";
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-berita',
  templateUrl: './berita.component.html',
  styleUrls: ['./berita.component.scss']
})
export class BeritaComponent implements OnInit {

  editForm: FormGroup;
  editForm2: FormGroup;

  private errorMessage;
  private currentId;

  News: any = [];

  beritaId = new FormControl('', Validators.required);
  judul = new FormControl('', Validators.required);
  deskripsi = new FormControl('', Validators.required);

  BeritaId: string;
  Judul: string;
  Gambar: string;
  Deskripsi: string;

  constructor(private router:Router, public fileUploadService: FileUploadService, fb: FormBuilder, private actRoute: ActivatedRoute) {
    this.getNews();
    this.editForm = fb.group({
      beritaId: this.beritaId,
      judul: this.judul,
      deskripsi: this.deskripsi
    });
  }

  ngOnInit() { }

  getNews() {
    this.fileUploadService.getNews().subscribe((res) => {
      this.News = res['news'];
    })
  }

  getDetailNews(id) {
    this.fileUploadService.getSingleNews(id).subscribe(data => {
      this.BeritaId = data['_id'];
      this.Judul = data['judul'];
      this.Gambar = data['gambar'];
      this.Deskripsi = data['deskripsi'];
    })
  }

  getSingleNews(id){

    this.fileUploadService.getSingleNews(id).subscribe(data => {
    
      console.log('ini data judul: ' + data['_id']);
      this.editForm.patchValue({
        beritaId: data['_id'],
        judul: data['judul'],
        deskripsi: data['deskripsi'],
      });
    });
  }

  updateNews() {;
    let id = this.editForm.value.beritaId;
    let judul = this.editForm.value.judul;
    let deskripsi = this.editForm.value.deskripsi;

    console.log('update news judul: ' +judul);
    this.fileUploadService.updateNews(id, judul, deskripsi).subscribe(
      (res) => {
        console.log('News successfully created!');
        this.getNews();
      }, (error) => {
        console.log(error);
      });;
  }

  setId(id) {
    this.currentId = id;
  }

  deleteNews(id) {
    this.fileUploadService.deleteNews(id).subscribe(
      (res) => {
        console.log('News successfully deleted!');
        this.getNews();
      }, (error) => {
        console.log(error);
      });;
  }

}
