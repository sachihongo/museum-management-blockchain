import { Injectable } from '@angular/core';
import { News } from './news';
import { Observable } from 'rxjs';
import { _throw } from 'rxjs/observable/throw';
import { catchError, map } from 'rxjs/operators';
import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable()
export class FileUploadService {

  baseURL = "http://localhost:8000/api";
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Get News
  getNews() {
    return this.http.get(this.baseURL)
  }

  // Get Single News
  getSingleNews(id): Observable<any> {
    console.log('Halo babi' + id);
    return this.http.get(`${this.baseURL}/${id}`).pipe(
      map((res: Response) => {
        return res || {}
      }),
      catchError(this.errorMgmt)
    );
  }

  // Create News
  addNews(judul: string, profileImage: File, deskripsi: string): Observable<any> {
    var formData: any = new FormData();
    formData.append("judul", judul);
    formData.append("gambar", profileImage);
    formData.append("deskripsi", deskripsi);

    return this.http.post<News>(`${this.baseURL}/create-news`, formData, {
      reportProgress: true,
      observe: 'events'
    })
  }

  // Update News
  updateNews(id, judul, deskripsi): Observable<any> {
    
    console.log('Lagi pengen update nig' + id);

    return this.http.put(`${this.baseURL}/update-news/${id}`, { judul, deskripsi }, {
      reportProgress: true,
      observe: 'events'
    })
  }


  // Delete News
  deleteNews(id): Observable<any> {

    return this.http.delete<News>(`${this.baseURL}/delete-news/${id}`, {
      reportProgress: true,
      observe: 'events'
    })
  }


  // Error handling 
  errorMgmt(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return _throw(errorMessage);
  }

}