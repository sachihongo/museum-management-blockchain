/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { WebhomeComponent } from './layouts/user-layout/webhome/webhome.component';
import { WebberitaComponent } from './layouts/user-layout/webberita/webberita.component';
import { WebkoleksiComponent } from './layouts/user-layout/webkoleksi/webkoleksi.component';
import { WebmenuComponent } from './layouts/user-layout/webmenu/webmenu.component';


const routes: Routes = [
    {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'webhome',
    component: WebhomeComponent
  },
  {
    path: 'webmenu',
    component: WebmenuComponent
  },
  {
    path: 'webberita',
    component: WebberitaComponent
  },
  {
    path: 'webkoleksi/:id',
    component: WebkoleksiComponent
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, 
  {
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
      path: '',
      loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
        }]
  },
  {
    path: '**',
    redirectTo: 'webhome'
  }
];

@NgModule({
 imports: [RouterModule.forRoot(routes, {
   useHash: true
 })],
 exports: [RouterModule],
 providers: []
})
export class AppRoutingModule { }
