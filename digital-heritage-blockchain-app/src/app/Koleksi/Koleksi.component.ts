/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { KoleksiService } from './Koleksi.service';
import 'rxjs/add/operator/toPromise';

const URL = 'http://localhost:8000/api2/upload-image';

@Component({
  selector: 'app-koleksi',
  templateUrl: './Koleksi.component.html',
  styleUrls: ['./Koleksi.component.css'],
  providers: [KoleksiService]
})
export class KoleksiComponent implements OnInit {

  myForm: FormGroup;
  selectedFile: File

  private allAssets;
  private asset;
  private currentId;
  private errorMessage;

  public uploader: FileUploader = new FileUploader({
    url: URL,
    itemAlias: 'image'
  });


  koleksiId = new FormControl('', Validators.required);
  nama = new FormControl('', Validators.required);
  jumlah = new FormControl('', Validators.required);
  kondisi = new FormControl('', Validators.required);
  kategori = new FormControl('', Validators.required);
  keterangan = new FormControl('', Validators.required);
  tanggalbentuk = new FormControl('', Validators.required);
  status = new FormControl('', Validators.required);
  pemilik = new FormControl('', Validators.required);
  owner = new FormControl('', Validators.required);
  issuer = new FormControl('', Validators.required);

  currentDate = new Date();

  constructor(public serviceKoleksi: KoleksiService, fb: FormBuilder) {
    this.myForm = fb.group({
      koleksiId: this.koleksiId,
      nama: this.nama,
      jumlah: this.jumlah,
      kondisi: this.kondisi,
      kategori: this.kategori,
      keterangan: this.keterangan,
      tanggalbentuk: this.tanggalbentuk,
      status: this.status,
      pemilik: this.pemilik,
      owner: this.owner,
      issuer: this.issuer
    });
  };

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    console.log('Nama yang ingin dipanggil:' + this.selectedFile.name); ///this.selectedfile bisa dipanggil ke fungsi lainnya di luar ini
  }

  ngOnInit(): void {
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, status: any) => {
      console.log('Uploaded File Details:', item);
      console.log('File Upload Success');
    };
    this.loadAll();
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceKoleksi.getAll()
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        result.forEach(asset => {
          tempList.push(asset);
        });
        this.allAssets = tempList;

      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

  //For counting total aset

  totalCount: number;

  hitungKoleksi() {
    this.serviceKoleksi.getAll().subscribe(data => {
      this.totalCount = data.length;
      console.log('jumlah koleksi blockchain: ' + this.totalCount);
    })
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the asset field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the asset updateDialog.
   * @param {String} name - the name of the asset field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified asset field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  resourceO = 'resource:model.Organisasi#O001';
  parsing: any;
  parsing2: any;
  parsingK: any;
  parsingP: any;
  parsingO: any;

  addAsset(form: any): Promise<any> {
    this.parsing = this.pemilik.value;
    this.parsing2 = this.owner.value;
    this.asset = {
      $class: 'model.Koleksi',
      'koleksiId': this.koleksiId.value,
      'nama': this.nama.value,
      'jumlah': this.jumlah.value,
      'kondisi': this.kondisi.value,
      'kategori': this.kategori.value,
      'keterangan': this.keterangan.value,
      'tanggalbentuk': this.currentDate,
      'status': this.selectedFile.name,
      'owner': this.pemilik.value + this.owner.value,
      'issuer': this.resourceO
    };

    this.myForm.setValue({
      'koleksiId': null,
      'nama': null,
      'jumlah': null,
      'kondisi': null,
      'kategori': null,
      'keterangan': null,
      'tanggalbentuk': null,
      'status': null,
      'pemilik': null,
      'owner': null,
      'issuer': null
    });
    console.log('Halo' + this.parsing);
    console.log('Hai' + this.parsing2);

    switch (this.parsing) {
      case 'resource:model.Kolektor#K':
        this.parsingK = 'K' + this.parsing2;

        console.log('Add Data with Owner Kolektor');
        return this.serviceKoleksi.getKolektor(this.parsingK).toPromise()
          .then((result) => {
            this.errorMessage = null;
            console.log('Hasil respon: ' + result);
            return this.serviceKoleksi.addAsset(this.asset)
              .toPromise()
              .then(() => {
                this.errorMessage = null;
                this.myForm.setValue({
                  'koleksiId': null,
                  'nama': null,
                  'jumlah': null,
                  'kondisi': null,
                  'kategori': null,
                  'keterangan': null,
                  'tanggalbentuk': null,
                  'status': null,
                  'pemilik': null,
                  'owner': null,
                  'issuer': null
                });
                this.loadAll();
              })
              .catch((error) => {
                if (error === 'Server error') {
                  this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
                } else {
                  this.errorMessage = error;
                }
              });
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Tolong cek kembali komponen ID-Koleksi dan ID-Pemilik dan cek available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'resource:model.Pewaris#P':
        console.log('Add Data with Owner Pewaris');
        this.parsingK = 'P' + this.parsing2;

        console.log('Add Data with Owner Kolektor');
        return this.serviceKoleksi.getPewaris(this.parsingP).toPromise()
          .then((result) => {
            this.errorMessage = null;
            console.log('Hasil respon: ' + result);
            return this.serviceKoleksi.addAsset(this.asset)
              .toPromise()
              .then(() => {
                this.errorMessage = null;
                this.myForm.setValue({
                  'koleksiId': null,
                  'nama': null,
                  'jumlah': null,
                  'kondisi': null,
                  'kategori': null,
                  'keterangan': null,
                  'tanggalbentuk': null,
                  'status': null,
                  'pemilik': null,
                  'owner': null,
                  'issuer': null
                });
                this.loadAll();
              })
              .catch((error) => {
                if (error === 'Server error') {
                  this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
                } else {
                  this.errorMessage = error;
                }
              });
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Tolong cek kembali komponen ID-Koleksi dan ID-Pemilik dan cek available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'resource:model.Organisasi#O':
        console.log('Add Data with Owner Organisasi');
        this.parsingK = 'O' + this.parsing2;

        console.log('Add Data with Owner Kolektor');
        return this.serviceKoleksi.getOrganisasi(this.parsingO).toPromise()
          .then((result) => {
            this.errorMessage = null;
            console.log('Hasil respon: ' + result);
            return this.serviceKoleksi.addAsset(this.asset)
              .toPromise()
              .then(() => {
                this.errorMessage = null;
                this.myForm.setValue({
                  'koleksiId': null,
                  'nama': null,
                  'jumlah': null,
                  'kondisi': null,
                  'kategori': null,
                  'keterangan': null,
                  'tanggalbentuk': null,
                  'status': null,
                  'pemilik': null,
                  'owner': null,
                  'issuer': null
                });
                this.loadAll();
              })
              .catch((error) => {
                if (error === 'Server error') {
                  this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
                } else {
                  this.errorMessage = error;
                }
              });
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Tolong cek kembali komponen ID-Koleksi dan ID-Pemilik dan cek available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      default:
        console.log('Salah Parameter');
    }
  }


  updateAsset(form: any): Promise<any> {
    this.asset = {
      $class: 'model.Koleksi',
      'nama': this.nama.value,
      'jumlah': this.jumlah.value,
      'kondisi': this.kondisi.value,
      'kategori': this.kategori.value,
      'keterangan': this.keterangan.value,
      'tanggalbentuk': this.tanggalbentuk.value,
      'status': this.status.value,
      'owner': this.owner.value,
      'issuer': this.issuer.value
    };

    return this.serviceKoleksi.updateAsset(form.get('koleksiId').value, this.asset)
      .toPromise()
      .then(() => {
        this.errorMessage = null;
        this.loadAll();
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }


  deleteAsset(): Promise<any> {

    return this.serviceKoleksi.deleteAsset(this.currentId)
      .toPromise()
      .then(() => {
        this.errorMessage = null;
        this.loadAll();
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  getForm(id: any): Promise<any> {

    return this.serviceKoleksi.getAsset(id)
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        console.log('Halo');
        const formObject = {
          'koleksiId': null,
          'nama': null,
          'jumlah': null,
          'kondisi': null,
          'kategori': null,
          'keterangan': null,
          'tanggalbentuk': null,
          'status': null,
          'pemilik': null,
          'owner': null,
          'issuer': null
        };

        if (result.koleksiId) {
          formObject.koleksiId = result.koleksiId;
        } else {
          formObject.koleksiId = null;
        }

        if (result.nama) {
          formObject.nama = result.nama;
        } else {
          formObject.nama = null;
        }

        if (result.jumlah) {
          formObject.jumlah = result.jumlah;
        } else {
          formObject.jumlah = null;
        }

        if (result.kondisi) {
          formObject.kondisi = result.kondisi;
        } else {
          formObject.kondisi = null;
        }

        if (result.kategori) {
          formObject.kategori = result.kategori;
        } else {
          formObject.kategori = null;
        }

        if (result.keterangan) {
          formObject.keterangan = result.keterangan;
        } else {
          formObject.keterangan = null;
        }

        if (result.tanggalbentuk) {
          formObject.tanggalbentuk = result.tanggalbentuk;
        } else {
          formObject.tanggalbentuk = null;
        }

        if (result.status) {
          formObject.status = result.status;
        } else {
          formObject.status = null;
        }

        if (result.owner) {
          formObject.owner = result.owner;
        } else {
          formObject.owner = null;
        }

        if (result.issuer) {
          formObject.issuer = result.issuer;
        } else {
          formObject.issuer = null;
        }

        this.myForm.setValue(formObject);

      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

  KoleksiId: string;
  Nama: string;
  Jumlah: number;
  Kondisi: number;
  Kategori: string;
  Keterangan: string;
  Tanggalbentuk: string;
  Status: string;
  Owner: any;
  Issuer: any;
  link = 'http://localhost:8000/public/koleksi/'

  getSingleClick(id: any): Promise<any> {
    return this.serviceKoleksi.getAsset(id)
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        console.log(result);
        this.KoleksiId = result.koleksiId;
        this.Nama = result.nama;
        this.Jumlah = result.jumlah;
        this.Kondisi = result.kondisi;
        this.Kategori = result.kategori;
        this.Keterangan = result.keterangan;
        this.Tanggalbentuk = result.tanggalbentuk;
        this.Status = this.link + result.status;
        this.Owner = result.owner;
        this.Issuer = result.issuer;

      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }


  resetForm(): void {
    this.myForm.setValue({
      'koleksiId': null,
      'nama': null,
      'jumlah': null,
      'kondisi': null,
      'kategori': null,
      'keterangan': null,
      'tanggalbentuk': null,
      'status': null,
      'pemilik': null,
      'owner': null,
      'issuer': null
    });
  }

}
