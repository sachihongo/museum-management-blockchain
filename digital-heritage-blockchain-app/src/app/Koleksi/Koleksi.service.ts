/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs/Observable';
import { Koleksi, Kolektor, Pewaris, Organisasi } from '../model';
import 'rxjs/Rx';

// Can be injected into a constructor
@Injectable()
export class KoleksiService {

  private NAMESPACE = 'Koleksi';
  private KOLEKTOR = 'Kolektor';
  private PEWARIS = 'Pewaris';
  private ORGANISASI = 'Organisasi';

  constructor(private dataService: DataService<Koleksi>,
    private dataService2: DataService<Kolektor>,
    private dataService3: DataService<Pewaris>,
    private dataService4: DataService<Organisasi>) {
  };

  public getAll(): Observable<Koleksi[]> {
    return this.dataService.getAll(this.NAMESPACE);
  }

  public getAsset(id: any): Observable<Koleksi> {
    return this.dataService.getSingle(this.NAMESPACE, id);
  }

  public getKolektor(id: any): Observable<Kolektor> {
    return this.dataService2.getSingle(this.KOLEKTOR, id);
  }

  public getPewaris(id: any): Observable<Pewaris> {
    return this.dataService3.getSingle(this.PEWARIS, id);
  }

  public getOrganisasi(id: any): Observable<Organisasi> {
    return this.dataService4.getSingle(this.ORGANISASI, id);
  }

  public addAsset(itemToAdd: any): Observable<Koleksi> {
    return this.dataService.add(this.NAMESPACE, itemToAdd);
  }

  public updateAsset(id: any, itemToUpdate: any): Observable<Koleksi> {
    return this.dataService.update(this.NAMESPACE, id, itemToUpdate);
  }

  public deleteAsset(id: any): Observable<Koleksi> {
    return this.dataService.delete(this.NAMESPACE, id);
  }

}
