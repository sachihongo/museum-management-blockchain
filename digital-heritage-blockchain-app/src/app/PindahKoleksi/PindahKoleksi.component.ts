/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PindahKoleksiService } from './PindahKoleksi.service';
import 'rxjs/add/operator/toPromise';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pindahkoleksi',
  templateUrl: './PindahKoleksi.component.html',
  styleUrls: ['./PindahKoleksi.component.css'],
  providers: [PindahKoleksiService]
})
export class PindahKoleksiComponent implements OnInit {

  myForm: FormGroup;

  private allTransactions;
  private Transaction;
  private currentId;
  private errorMessage;

  koleksi = new FormControl('', Validators.required);
  pemilik = new FormControl('', Validators.required)
  newOwner = new FormControl('', Validators.required);
  issuer = new FormControl('', Validators.required);
  transactionId = new FormControl('', Validators.required);
  timestamp = new FormControl('', Validators.required);


  constructor(private servicePindahKoleksi: PindahKoleksiService, fb: FormBuilder, private router: Router) {
    this.myForm = fb.group({
      koleksi: this.koleksi,
      pemilik: this.pemilik,
      newOwner: this.newOwner,
      issuer: this.issuer,
      transactionId: this.transactionId,
      timestamp: this.timestamp
    });
  };

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.servicePindahKoleksi.getAll()
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        result.forEach(transaction => {
          tempList.push(transaction);
        });
        this.allTransactions = tempList;
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the transaction field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the transaction updateDialog.
   * @param {String} name - the name of the transaction field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified transaction field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  resource0 = 'resource:model.Koleksi#'
  resource1 = 'resource:model.Organisasi#O001';
  parsing: any;
  parsing2: any;
  parsingK: any;
  parsingP: any;
  parsingO: any;

  addTransaction(form: any): Promise<any> {
    this.parsing = this.pemilik.value;
    this.parsing2 = this.newOwner.value;

    this.Transaction = {
      $class: 'model.PindahKoleksi',
      'koleksi': this.resource0 + this.koleksi.value,
      'newOwner': this.pemilik.value + this.newOwner.value,
      'issuer': this.resource1,
      'transactionId': this.transactionId.value,
      'timestamp': this.timestamp.value
    };

    this.myForm.setValue({
      'koleksi': null,
      'pemilik': null,
      'newOwner': null,
      'issuer': null,
      'transactionId': null,
      'timestamp': null
    });

    switch (this.parsing) {
      case 'resource:model.Kolektor#K':
        this.parsingK = 'K' + this.parsing2;

        console.log('Add Data with newOwner Kolektor');
        return this.servicePindahKoleksi.getKolektor(this.parsingK).toPromise()
          .then((result) => {
            return this.servicePindahKoleksi.addTransaction(this.Transaction)
              .toPromise()
              .then(() => {
                this.errorMessage = null;
                this.myForm.setValue({
                  'koleksi': null,
                  'pemilik': null,
                  'newOwner': null,
                  'issuer': null,
                  'transactionId': null,
                  'timestamp': null
                });
                this.router.navigate(['/koleksi']);
              })
              .catch((error) => {
                if (error === 'Server error') {
                  this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
                } else {
                  this.errorMessage = error;
                }
              });
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Tolong cek kembali komponen ID-Koleksi dan ID-Pemilik dan cek available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'resource:model.Pewaris#P':
        this.parsingP = 'P' + this.parsing2;

        console.log('Add Data with newOwner Pewaris');
        return this.servicePindahKoleksi.getKolektor(this.parsingP).toPromise()
          .then((result) => {
            return this.servicePindahKoleksi.addTransaction(this.Transaction)
              .toPromise()
              .then(() => {
                this.errorMessage = null;
                this.myForm.setValue({
                  'koleksi': null,
                  'pemilik': null,
                  'newOwner': null,
                  'issuer': null,
                  'transactionId': null,
                  'timestamp': null
                });
                this.router.navigate(['/koleksi']);
              })
              .catch((error) => {
                if (error === 'Server error') {
                  this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
                } else {
                  this.errorMessage = error;
                }
              });
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Tolong cek kembali komponen ID-Koleksi dan ID-Pemilik dan cek available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      case 'resource:model.Organisasi#O':
        this.parsingK = 'O' + this.parsing2;

        console.log('Add Data with newOwner Kolektor');
        return this.servicePindahKoleksi.getKolektor(this.parsingO).toPromise()
          .then((result) => {
            return this.servicePindahKoleksi.addTransaction(this.Transaction)
              .toPromise()
              .then(() => {
                this.errorMessage = null;
                this.myForm.setValue({
                  'koleksi': null,
                  'pemilik': null,
                  'newOwner': null,
                  'issuer': null,
                  'transactionId': null,
                  'timestamp': null
                });
                this.router.navigate(['/koleksi']);
              })
              .catch((error) => {
                if (error === 'Server error') {
                  this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
                } else {
                  this.errorMessage = error;
                }
              });
          })
          .catch((error) => {
            if (error === 'Server error') {
              this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
            } else if (error === '404 - Not Found') {
              this.errorMessage = '404 - Could not find API route. Tolong cek kembali komponen ID-Koleksi dan ID-Pemilik dan cek available APIs.';
            } else {
              this.errorMessage = error;
            }
          });
      default:
        console.log('Salah Parameter');
    }
  }

  updateTransaction(form: any): Promise<any> {
    this.Transaction = {
      $class: 'model.PindahKoleksi',
      'koleksi': this.koleksi.value,
      'newOwner': this.newOwner.value,
      'issuer': this.issuer.value,
      'timestamp': this.timestamp.value
    };

    return this.servicePindahKoleksi.updateTransaction(form.get('transactionId').value, this.Transaction)
      .toPromise()
      .then(() => {
        this.errorMessage = null;
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

  deleteTransaction(): Promise<any> {

    return this.servicePindahKoleksi.deleteTransaction(this.currentId)
      .toPromise()
      .then(() => {
        this.errorMessage = null;
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  getForm(id: any): Promise<any> {

    return this.servicePindahKoleksi.getTransaction(id)
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        const formObject = {
          'koleksi': null,
          'pemilik': null,
          'newOwner': null,
          'issuer': null,
          'transactionId': null,
          'timestamp': null
        };

        if (result.koleksi) {
          formObject.koleksi = result.koleksi;
        } else {
          formObject.koleksi = null;
        }

        if (result.newOwner) {
          formObject.newOwner = result.newOwner;
        } else {
          formObject.newOwner = null;
        }


        if (result.issuer) {
          formObject.issuer = result.issuer;
        } else {
          formObject.issuer = null;
        }


        if (result.transactionId) {
          formObject.transactionId = result.transactionId;
        } else {
          formObject.transactionId = null;
        }

        if (result.timestamp) {
          formObject.timestamp = result.timestamp;
        } else {
          formObject.timestamp = null;
        }

        this.myForm.setValue(formObject);

      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else if (error === '404 - Not Found') {
          this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
        } else {
          this.errorMessage = error;
        }
      });
  }

  resetForm(): void {
    this.myForm.setValue({
      'koleksi': null,
      'pemilik': null,
      'newOwner': null,
      'issuer': null,
      'transactionId': null,
      'timestamp': null
    });
  }
}
