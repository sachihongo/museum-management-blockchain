import { Component, OnInit, ComponentFactoryResolver, ViewEncapsulation } from '@angular/core';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { LegendItem, ChartType } from '../lbd/lbd-chart/lbd-chart.component';
import { Dashboard } from './dashboard.service';
import * as Chartist from 'chartist';
import { AnimationMetadataType } from '@angular/core/src/animation/dsl';
import { map } from 'rxjs/operators';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [Dashboard],
  encapsulation: ViewEncapsulation.None
})

export class HomeComponent implements OnInit {
  public emailChartType: ChartType;
  public emailChartData: any;
  public emailChartOptions: any;
  public emailChartLegendItems: LegendItem[];

  public activityChartType: ChartType;
  public activityChartData: any;
  public activityChartOptions: any;
  public activityChartResponsive: any[];
  public activityChartLegendItems: LegendItem[];


  asyncResult: any;

  berita: number;
  kolektor: number;
  pewaris: number;
  organisasi: number;

  historika: number;
  etnografika: number;
  grafika: number;
  senirupa: number;
  numismatika: number;
  biologika: number;
  filologika: number;
  arkeologika: number;
  keramologika: number;
  geografika: number;


  async getBerita() {
    this.asyncResult = await this.serviceDashboard.getAllBerita().toPromise().then(data => {
      return data['news'].length;
    });
    return this.asyncResult;
  }


  constructor(public serviceDashboard: Dashboard) { }


  async ngOnInit() {
    console.log('Halo from ngOnInit');
    
    const berita = await this.getBerita();
    const kolektor = await this.serviceDashboard.getAllKolektor().toPromise();
    const pewaris = await this.serviceDashboard.getAllPewaris().toPromise();
    const organisasi = await this.serviceDashboard.getAllOrganisasi().toPromise();


    const historika = await this.serviceDashboard.getAllHistorika().toPromise();
    const etnografika = await this.serviceDashboard.getAllEtnografika().toPromise();
    const grafika = await this.serviceDashboard.getAllGrafika().toPromise();
    const senirupa = await this.serviceDashboard.getAllSeniRupa().toPromise();
    const numismatika = await this.serviceDashboard.getAllNumismatika().toPromise();
    const biologika = await this.serviceDashboard.getAllBiologika().toPromise();
    const filologika = await this.serviceDashboard.getAllFilologika().toPromise();
    const arekeologika = await this.serviceDashboard.getAllArkeologika().toPromise();
    const keramologika = await this.serviceDashboard.getAllKeramologika().toPromise();
    const geografika = await this.serviceDashboard.getAllGeografika().toPromise();
   
    this.berita = berita;
    this.kolektor = kolektor.length;
    this.pewaris = pewaris.length;
    this.organisasi = organisasi.length;

    this.historika = historika.length;
    this.etnografika = etnografika.length;
    this.grafika = grafika.length;
    this.senirupa = senirupa.length;
    this.numismatika = numismatika.length;
    this.biologika = biologika.length;
    this.filologika = filologika.length;
    this.arkeologika = arekeologika.length;
    this.keramologika = keramologika.length;
    this.geografika = geografika.length;


    //Chart Donat
    this.emailChartType = ChartType.Pie;
    this.emailChartData = {
      series: [this.historika, this.etnografika, this.grafika, this.senirupa, this.numismatika, this.biologika, this.filologika, this.arkeologika, this.keramologika, this.geografika]
    };
    this.emailChartOptions = {donut: true},
    this.emailChartLegendItems = [
      { title: 'His', imageClass: 'fa fa-circle historika' },
      { title: 'Etn', imageClass: 'fa fa-circle etnografika' },
      { title: 'Grf', imageClass: 'fa fa-circle grafika' },
      { title: 'Snr', imageClass: 'fa fa-circle senirupa' },
      { title: 'Num', imageClass: 'fa fa-circle numismatika' },
      { title: 'Bio', imageClass: 'fa fa-circle biologika' },
      { title: 'Fil', imageClass: 'fa fa-circle filologika' },
      { title: 'Ark', imageClass: 'fa fa-circle arkeologika' },
      { title: 'Krm', imageClass: 'fa fa-circle keramologika' },
      { title: 'Geo', imageClass: 'fa fa-circle geografika' }
    ];

    //Chart Bar
    this.activityChartType = ChartType.Bar;
      this.activityChartData = {
        labels: ['His', 'Etn', 'Grf', 'Snr', 'Num', 'Bio', 'Fil', 'Ark', 'Krm', 'Geo'],
        series: [
          [this.historika, this.etnografika, this.grafika, this.senirupa, this.numismatika, this.biologika, this.filologika, this.arkeologika, this.keramologika, this.geografika]
        ]
      };
    

  }
}
