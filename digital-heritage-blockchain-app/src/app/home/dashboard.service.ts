/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs/Observable';
import { Koleksi, Kolektor, Pewaris, Organisasi } from '../model';
import 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
// Can be injected into a constructor
@Injectable()
export class Dashboard {
  baseURL = "http://localhost:8000/api";

  private HISTORIKA = 'queries/selectAssetByKategoriHistorika';
  private ETNOGRAFIKA = 'queries/selectAssetByKategoriEtnografika';
  private GRAFIKA = 'queries/selectAssetByKategoriGrafika';
  private SENIRUPA = 'queries/selectAssetByKategoriSeniRupa';
  private NUMISMATIKA = 'queries/selectAssetByKategoriNumismatika';
  private BIOLOGIKA = 'queries/selectAssetByKategoriBiologika';
  private FILOLOGIKA = 'queries/selectAssetByKategoriFilologika';
  private ARKEOLOGIKA = 'queries/selectAssetByKategoriArkeologika';
  private KERAMOLOGIKA = 'queries/selectAssetByKategoriKeramologika';
  private GEOGRAFIKA = 'queries/selectAssetByKategoriGeografika';

  private KOLEKTOR = 'Kolektor';
  private PEWARIS = 'Pewaris';
  private ORGANISASI = 'Organisasi';

  constructor(
      private dataService1: DataService<Koleksi>,
      private dataService2: DataService<Kolektor>,
      private dataService3: DataService<Pewaris>,
      private dataService4: DataService<Organisasi>,
      private http: HttpClient) {
  };

  public getAllHistorika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.HISTORIKA);
  }

  public getAllEtnografika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.ETNOGRAFIKA);
  }

  public getAllGrafika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.GRAFIKA);
  }

  public getAllSeniRupa(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.SENIRUPA);
  }

  public getAllNumismatika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.NUMISMATIKA);
  }

  public getAllBiologika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.BIOLOGIKA);
  }

  public getAllFilologika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.FILOLOGIKA);
  }

  public getAllArkeologika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.ARKEOLOGIKA);
  }

  public getAllKeramologika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.KERAMOLOGIKA);
  }

  public getAllGeografika(): Observable<Koleksi[]> {
    return this.dataService1.getAll(this.GEOGRAFIKA);
  }

  public getAllKolektor(): Observable<Kolektor[]> {
    return this.dataService2.getAll(this.KOLEKTOR);
  }

  public getAllPewaris(): Observable<Pewaris[]> {
    return this.dataService3.getAll(this.PEWARIS);
  }

  public getAllOrganisasi(): Observable<Organisasi[]> {
    return this.dataService4.getAll(this.ORGANISASI);
  }

  public getAllBerita(): Observable<any>{
    return this.http.get(this.baseURL);
  }
}
