import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dasbor',  icon: 'pe-7s-graph', class: '' },
    { path: '/berita', title: 'Berita',  icon: 'pe-7s-news-paper', class: '' },
    { path: '/holder', title: 'Pemilik',  icon: 'pe-7s-user-female', class: '' },
    { path: '/koleksi', title: 'Koleksi',  icon: 'pe-7s-disk', class: '' },
    { path: '/pindahkoleksi', title: 'Pindah Kepemilikan',  icon: 'pe-7s-network', class: '' },
    { path: '/alltransaksi', title: 'Riwayat',  icon: 'pe-7s-refresh-cloud', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
