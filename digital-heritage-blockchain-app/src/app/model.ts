import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace model{
   export enum Kondisi {
      BAIK,
      RUSAK,
   }
   export class Koleksi extends Asset {
      koleksiId: string;
      nama: string;
      jumlah: number;
      kondisi: Kondisi;
      kategori: string;
      status: string;
      keterangan: string;
      tanggalbentuk: string;
      owner: Participant;
      issuer: Participant;
   }
   export class Kolektor extends Participant {
      kolektorId: string;
      namaPertama: string;
      namaTerakhir: string;
      keterangan: string;
      tipe: string;
   }
   export class Pewaris extends Participant {
      pewarisId: string;
      namaPertama: string;
      namaTerakhir: string;
      keterangan: string;
      tipe: string;
   }
   export class Organisasi extends Participant {
      organisasiId: string;
      nama: string;
      keterangan: string;
      tipe: string;
   }
   export class Pengguna extends Participant {
      email: string;
      namaPertama: string;
      namaTerakhir: string;
      password: string;
      tipe: string;
   }
   export class PindahKoleksi extends Transaction {
      koleksi: Koleksi;
      issuer: Participant;
      newOwner: Participant;
   }
// }
